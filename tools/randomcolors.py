from time import sleep
import serial
s = serial.Serial("/dev/rfcomm0",57600,timeout = 2)
intervalo=0.05
maxbright=15
minbright=1
while True:
    for i in range(minbright,maxbright):
        strval = str(i)+"."+str(i)+"."+str(i)+")"
        s.write(bytes(strval))
        print(strval)
        sleep(intervalo)
    for i in range(maxbright,minbright,-1):
        strval = str(i)+"."+str(i)+"."+str(i)+")"
        s.write(bytes(strval))
        print(strval)
        sleep(intervalo)
